---
title: Blogpost 6 september
date: 2017-9-6
---

#### **Studiodag 6 september** - CMD Design Challenge 1
---
>*Even samengevat...*
In deze les kregen we als groep de tijd om het onderzoek uit te breiden en verder te werken aan de Design Challenge. Elk teamlid vertelde wat ze hadden gemaakt en daarna konden we verder met ontbrekende informatie op zoeken. Daarna kregen we van Mio een les in het maken van de blog. Hierin werd uitgelegd hoe Markdown werkt en hoe we blogposts konden maken met een gestructureerde opmaak. Vervolgens kregen we een les van de studiecoaches en hebben we met het team de bijbehorende opdracht gemaakt tijdens de les.

---

**Wat heb ik onderzocht/gedaan?** Vandaag heb ik samen met de groep bekeken welke informatie we nog misten om verder te gaan met de Design Challenge. Ik ben van iedereen in het team het onderzoek terug gaan lezen om inzicht te krijgen in corresponderende informatie. Zo kon ik snel zien wat overeen kwam en waar nog aangevuld moest worden. Zo heb ik bijvoorbeeld toegevoegd aan het onderzoek waarom we niet voor de eerder gekozen thema's hebben gekozen maar voor 'kunst'. Ook heb ik gewerkt aan mijn individuele deliverables. Verder zijn we met het team begonnen aan een taakverdeling en planning.

**Hoe heb ik dat aangepakt?**  Ik ben gaan overleggen met mijn team wat we nog nodig hadden voor ons concept. Ook heb ik zelf nog kort research gedaan naar andere game concepten om een beeld te krijgen hoe volledig zo iets is.

**Welke methodes heb ik gebruikt?** Voor dit onderzoek was desk research voldoende. 

**Wat ga ik doen met de gekregen feedback?** Ik kreeg van onze peer coach Heleen antwoord op een vraag die ik had over mijn individuele deliverables. Zij kon mij vertellen hoe ik mijn onderzoek visueel kon maken, en daar ga ik mee aan de slag.

**Wat wil ik de volgende keer anders doen?** Ik wilde maandag graag dat we in deze les conclusies zouden trekken uit het onderzoek. Helaas merkten we dat er toch nog wat informatie nodig was om alles volledig te maken,  dus dit is verschoven naar een andere keer. Volgende keer hebben we een duidelijk planning en dan kunnen we zulke dingen tackelen.

**Wat wil ik de volgende keer hetzelfde doen?** Ik wil volgende keer op hetzelfde tempo doorwerken als dat we deze les hebben gedaan.

**Wat heb ik hiervan geleerd?** Ik heb geleerd dat je het beste helemaal aan het begin een strakke planning moet maken omdat je anders wellicht in tijdnood komt. Ook heb ik geleerd dat het zaak is om nu zo snel mogelijk het onderzoek compleet te maken zodat we snel verder kunnen met onze ideeën over het concept en het spel zelf.

**Wat wil ik in volgende les gaan doen?** Volgende les wil ik feedback vragen (niet per se vanuit mezelf maar vanuit de groep) om te weten of er nog dingen missen. Volgende les is maandag 11 september. Dan moeten we gelijk een stand up doen over ons concept, dus dat wil ik zo goed mogelijk voorbereiden. 



