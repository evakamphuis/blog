---
title: Blogpost 4-9-2017
date: 2017-9-4
---


**Studiodag 4 september** - CMD Design Challenge 1
---
>*Even samengevat...*
In deze les kregen we als groep de tijd om een doelgroep te kiezen voor de opdracht. De briefing hadden we al ontvangen vorige week.  Verder was er deze les tijd om de doelgroep te kiezen. Dit is uiteindelijk de groep 'eerstejaars studenten van de Hogeschool Rotterdam die in bezit zijn van een Museumkaart'. Ook werd het thema vastgesteld voor de game, namelijk 'kunst'. Dit werd allebei gauw duidelijk waardoor er gelijk een taakverdeling kwam wat betreft het vooronderzoek. Individueel kozen we onderwerpen waar onderzoek naar gedaan ging worden. 

---

**Wat heb ik onderzocht/gedaan?** Mijn taak was het onderzoeken naar de Museumkaart, en naar paper-prototyping. Hier heb ik voor beide onderwerpen deskresearch naar gedaan.

**Hoe heb ik dat aangepakt?**  Ik heb eerst per onderwerp deelvragen bedacht. Dit omdat ik dan gerichter research kon doen. Toen ben ik op Google gaan zoeken naar informatie en zette ik dit in de gedeelde Google Drive van de groep. Als ik ergens tegenaan liep vroeg ik mijn groepsgenoten om hulp.

**Welke methodes heb ik gebruikt?** Voor dit vooronderzoek was desk research voldoende.

**Wat ga ik doen met de gekregen feedback?** Ik heb in deze les geen persoonlijke feedback ontvangen (ook niet gevraagd).

**Wat wil ik de volgende keer anders doen?** Ik wil graag de volgende keer gelijk feedback vragen naar mijn gedane werk, omdat ik nu op deze manier misschien wel denk dat ik in de goede richting zit maar dit misschien helemaal niet zo is. 

**Wat wil ik de volgende keer hetzelfde doen?** Hoe we in de groep samenwerken gaat heel goed; we laten iedereen in zijn waarde en luisteren goed naar elkaar. Elk groepslid heeft iets te bieden en dat benutten we goed. 

**Wat heb ik hiervan geleerd?** Ik heb geleerd dat ik in het vervolg sneller feedback moet vragen voor iets, omdat je alleen dan weet of je op het goede spoor zit. Niet per se over het onderzoek wat ik heb gedaan, maar meer het verloop van de opdracht zelf.  Ook heb ik geleerd dat duidelijkheid creeeren in de groep heel belangrijk is. Iedereen moet weten waar hij/zij op dat moment mee bezig is en met welk doel. 

**Wat wil ik in volgende les gaan doen?** Volgende les wil ik conclusies trekken uit het onderzoek wat ik gedaan heb in deze les en thuis. Verder wil ik meer vragen gaan stellen aan de aanwezige docenten en/of peer coaches. Ook hoop ik met de groep meer duidelijkheid te krijgen welke richting we op gaan met het project, en een duidelijke taakverdeling en planning maken zodat we met z'n allen weten waar we aan toe zijn de komende weken.



